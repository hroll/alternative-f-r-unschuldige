 /* A simple HTTPS server */
#include "common.h"
#include "server.h"

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

const char* guiHTMLKopf=
"<html>"
"<title>Alternative F&uuml;r Unschuldige</title>"

"<h1>Alternative F&uuml;r Unschuldige</h1>" 
"<br>"
"<br>"
"<br>";


const char* guiHTMLSchwanz=
"<form method=\"get\" action=\"sendeNachricht\">"
"Dein Name: <input type=\"text\" id=\"name\" name=\"name\" size=\"25\" value=\"####0\" /> <br />"
"Dein Passwort: <input type=\"text\" id=\"passwort\" name=\"passwort\" size=\"25\" value=\"####1\" /> <br />"
"Deine Nachricht: <input type=\"text\" id=\"nachricht\" name=\"nachricht\" size=\"300\" /> <br />"
"<input type=\"submit\" value=\"teile mit\" />"
"</form>"
"Dieses Programm stammt von der Wildsau-Gruppe und jeder darf es &auml;ndern und nutzen."
"</html>";

const int GETPufferLaenge=1000;

template<unsigned int hoechst>
class SicherZahl
{
   unsigned int _wertUngeschuetzt;
public:
   SicherZahl(unsigned int anfang) 
   {
      if( anfang > hoechst ) 
      {
        printf("beende mich geordnet bevor Angriff erfolgreich\n");
        exit(-1);
      }
      _wertUngeschuetzt = anfang;
   }
   void operator++()
   {
       if( _wertUngeschuetzt == hoechst )
       {
         printf("beende mich geordnet bevor Angriff erfolgreich\n");
         exit(-1);
       } 
       _wertUngeschuetzt++;
   }
   operator const int ()
   {
      return _wertUngeschuetzt;
   }

   void operator=(unsigned int neuerWert)
   {
       _wertUngeschuetzt = neuerWert;  
   } 

   void operator +=(unsigned int dazu)
   {
      unsigned int alt = _wertUngeschuetzt;
      _wertUngeschuetzt += dazu;
      if( _wertUngeschuetzt > hoechst || (dazu > 0 && _wertUngeschuetzt < alt) )
      {
         printf("beende mich geordnet bevor Angriff erfolgreich\n");
         exit(-1);
      }
   }
   void operator -=(unsigned int weg)
   {
       
      if( weg > _wertUngeschuetzt )
      {
         printf("beende mich geordnet bevor Angriff erfolgreich\n");
         exit(-1);
      }
      _wertUngeschuetzt -= weg;
   }
};


using namespace std;

struct UbkNameWert
{
   string _name;
   string _wert;
};

void ersetzePlatzhalter(const string& eingabe,
                        const char* platzhalter,
                        const char* ersatz,
                        string& ausgabe)
{
    SicherZahl<10000> stelle = 0;
    unsigned int lPH = strlen( platzhalter );
    while(stelle < eingabe.size() )
    {
       string pruef = eingabe.substr( stelle,lPH);
       if( pruef == platzhalter )
       {
          ausgabe.append(ersatz);
          stelle += (lPH -1);
       }
       else
       {
          ausgabe += eingabe.at(stelle);
       }
       ++stelle;
    }
}

void leseUnbekannteAusGET(const string getAnfrage,vector<UbkNameWert>& ausgabe)
{
    SicherZahl<2000> lz(0);//lesezeiger
    while( lz < getAnfrage.size() && getAnfrage.at(lz) != '?' ) 
    {
        ++lz;
    }
    ++lz;
    SicherZahl<21> ubkZeiger(0);
    ausgabe.resize( ausgabe.size() + 1);

    while( lz < getAnfrage.size() ) 
    {
        ausgabe.resize( ausgabe.size() + 1);
        while( lz < getAnfrage.size() && getAnfrage.at(lz) != '=' ) 
        {
             ausgabe[ubkZeiger]._name += getAnfrage.at(lz);
             ++lz;
        } 
        ++lz;  
        while( lz < getAnfrage.size() && getAnfrage.at(lz) != '&' && getAnfrage.at(lz) != ' ') 
        {
             ausgabe[ubkZeiger]._wert += getAnfrage.at(lz);
             ++lz;
        } 
        ++lz;
        ++ubkZeiger;
        if( ausgabe.size() > 20 )
        {
          return;
        }
        
    } 
}

struct BenutzerEinzelheiten
{
  string _name;
  string _passwort;
};

vector<BenutzerEinzelheiten> __benutzerListe;

 
 
 

extern "C" int berr_exit(char*);

const int __anzahlMeldungen = 100;
 
 


static int http_serve(SSL *ssl,int s)
  {
    char getBuffer[GETPufferLaenge];
    char buf[BUFSIZZ];
    int r,len;
    BIO *io,*ssl_bio;
    getBuffer[0]=0;
    
    io=BIO_new(BIO_f_buffer());
    ssl_bio=BIO_new(BIO_f_ssl());
    BIO_set_ssl(ssl_bio,ssl,BIO_CLOSE);
    BIO_push(io,ssl_bio);
    
    vector<UbkNameWert> unbekannteAusGET;
    while(1){
      r=BIO_gets(io,buf,BUFSIZZ-1);

      switch(SSL_get_error(ssl,r)){
        case SSL_ERROR_NONE:
          len=r;
          break;
        default:
          berr_exit("SSL read problem");
      }

      /* Look for the blank line that signals
         the end of the HTTP headers */
      if(!strcmp(buf,"\r\n") ||
        !strcmp(buf,"\n"))
        break;
      if( strstr( buf,"GET") == buf ) 
      {
          cout << "stelle 1" <<buf <<endl;
          strncpy(getBuffer,buf,GETPufferLaenge);
          getBuffer[GETPufferLaenge - 1] = 0;
          string getAlsString( getBuffer );
          leseUnbekannteAusGET( getAlsString, unbekannteAusGET ); 
      }
    }

    if((r=BIO_puts
      (io,"HTTP/1.0 200 OK\r\n"))<=0)
      err_exit("Write error");

    

    if((r=BIO_puts
      (io,"Server: EKRServer\r\n\r\n"))<=0)
      err_exit("Write error");

    if((r=BIO_puts
      (io,guiHTMLKopf))<=0)
      err_exit("Write error");


    string benutzer;
    string passwort;
    string nachricht;
    for(unsigned int i=0; i < unbekannteAusGET.size(); i++)
    {
        if( unbekannteAusGET.at(i)._name == "name" )
        {
            benutzer =  unbekannteAusGET.at(i)._wert;
        }
        if( unbekannteAusGET.at(i)._name == "passwort" )
        {
            passwort =  unbekannteAusGET.at(i)._wert;
        }
        if( unbekannteAusGET.at(i)._name == "nachricht" )
        {
            nachricht =  unbekannteAusGET.at(i)._wert;
        }
    }

    //Panzer gegen html-Einspritzen
    for(unsigned int i=0; i < nachricht.size();i++)
    {
        char z = nachricht.at(i);
        if( !( (z >= 'a' && z <= 'z') || 
               (z >= 'A' && z <= 'Z') ||
               (z >= '0' && z <= '9') ||
               (z == '%' ) ||
               (z == '.' ) ||
               (z == '+' )
             )
          )
        {
            nachricht="Abpraller.";
            break;
        }
    }

    //pruefe Name/Passwort
    bool paroleStimmt(false);
    for(unsigned int i=0; i < __benutzerListe.size(); i++)
    {
       cout << benutzer << " " <<  passwort << " " << 
               __benutzerListe.at(i)._name << " " << __benutzerListe.at(i)._passwort << endl;
       if( __benutzerListe.at(i)._name == benutzer && 
           __benutzerListe.at(i)._passwort == passwort )
       {
          paroleStimmt = true;
       }
    }
    
   
     
    /*while(  stelleZurAusgabe != __meldungsStelle )
    {
       BIO_puts(io,__meldungen[ stelleZurAusgabe ].c_str() ) ; 
       BIO_puts(io,"<br/>" ) ;
       ++stelleZurAusgabe;
    }*/

    

    /*for( uint i=0; i < unbekannteAusGET.size(); i++)
    {
        BIO_puts(io,unbekannteAusGET.at(i)._name.c_str() ) ; 
        BIO_puts(io," " ) ; 
        BIO_puts(io,unbekannteAusGET.at(i)._wert.c_str() ) ; 
        BIO_puts(io,"<br/>" ) ; 
    }*/

    string schwanzMitErsatz;
    ersetzePlatzhalter(guiHTMLSchwanz, "####0",benutzer.c_str(),schwanzMitErsatz);   
    string schwanzMitErsatz2;
    ersetzePlatzhalter(schwanzMitErsatz.c_str(), "####1",passwort.c_str(),schwanzMitErsatz2); 

    if((r=BIO_puts
      (io,schwanzMitErsatz2.c_str() ))<=0)
      err_exit("Write error");


    if( paroleStimmt )
    {
        if( nachricht.size() > 0 )
        {
             ofstream meldDatei("meldungen.txt",ofstream::out | ofstream::app);
             meldDatei << nachricht << "\n";
             meldDatei.close();
        }
        //jetzt noch vorhandene Meldungen schreiben
        string zeile;
        ifstream eingabeZaehler ("meldungen.txt");
        SicherZahl<1000000> zaehler(0);
        if (eingabeZaehler.is_open())
        {
           
           while ( eingabeZaehler.good() )
           {
             getline (eingabeZaehler,zeile);
             //BIO_puts(io,zeile
             ++zaehler;
           }
           eingabeZaehler.close();
        }   
        cout << "anzahl zeilen:"<< zaehler<<endl;
        //gebe nur die letzten 1000 Meldungen aus
        ifstream eingabeLeser ("meldungen.txt");
        vector<string> zeilen;
        zeilen.reserve(1000);
        if (eingabeLeser.is_open())
        {
           //cout <<"P1"<<endl;
           SicherZahl<1000000> abZeile(zaehler);
           SicherZahl<1000000> stelle(0);
           if( abZeile > 1000 )
           {
               abZeile -= 1000;
           }
           else
           {
              abZeile = 0;
           }
           cout <<" ab Zeile " << abZeile << endl;
           while ( eingabeLeser.good() )
           {
             //cout <<"P2"<<endl;
             getline (eingabeLeser,zeile);
             
             if( stelle >= abZeile )
             {
                zeilen.push_back( zeile );
             }
             ++stelle;
           }
           eingabeLeser.close();
           for(int i=zeilen.size()-1;i >= 0; i--)
           {
              BIO_puts(io,zeilen.at(i).c_str() );
              BIO_puts(io,"<br/>" );             
           }
        }     
    }
    else
    {
        if( __benutzerListe.size() > 0 )
        {
           BIO_puts(io,"<br/><b>Wir kennen Sie nicht</b>");
        }
        else
        {
           BIO_puts(io,"<br/><b>Es sind noch keine Nutzer angelegt.</b><br/>");
           BIO_puts(io,"Erzeugen sie dazu eine Datei nutzer.txt. Beispiel:<br/>");
           BIO_puts(io,"Piet:RostigeQuelle<br/>");
           BIO_puts(io,"Liesa:Pusteblume<br/>");
           BIO_puts(io,"Fritz:Schweinebraten<br/>");
           BIO_puts(io,"Abdullah:SchweineschmalzKryptoBringtKeineJungfrauen<br/>");
        }
    } 

    
    
 
    if((r=BIO_puts
      (io,"\r\n"))<=0)
      err_exit("Write error");
    
    if((r=BIO_flush(io))<0)
      err_exit("Error flushing BIO");


    
    r=SSL_shutdown(ssl);
    if(!r){
      /* If we called SSL_shutdown() first then
         we always get return value of '0'. In
         this case, try again, but first send a
         TCP FIN to trigger the other side's
         close_notify*/
      shutdown(s,1);
      r=SSL_shutdown(ssl);
    }
      
    switch(r){  
      case 1:
        break; /* Success */
      case 0:
      case -1:
      default:
        berr_exit("Shutdown failed");
    }

    SSL_free(ssl);
    close(s);

    return(0);
  }
 
int main(int argc,char **argv)
  {
   
    int sock,s;
    BIO *sbio;
    SSL_CTX *ctx;
    SSL *ssl;
    int r;
    pid_t pid;

    //erzeuge das SSL/TLS Schluesselmaterial
    int egal;
    if( open("zertifikat.pem",O_RDONLY) < 0)
    {
         cout<<"Erzeuge Schluesselmaterial, da noch nicht vorhanden"<<endl;
         system("openssl genrsa -out schluessel.pem 4096");
         system("openssl req -new -key schluessel.pem -out zertifikatAnforderung.pem "
                "-subj \"/countryName=DE/localityName=Schnueffelshausen/"        
                "organizationalUnitName=UnterwaescheInspektion/commonName=geht.niemand.was.an\"");
         system("openssl x509 -req -days 30 -in zertifikatAnforderung.pem "
                "-signkey schluessel.pem -out zertifikat.pem");
    }
    

   
     

    int henkelNL=open("nutzer.txt",O_RDONLY);
    if( henkelNL >= 0 )
    {
       string nameGelesen;
       string passwortGelesen; 
       bool zustandName=true;
       char puffer[1];
       while(read(henkelNL,puffer,1) == 1)
       {
           switch(puffer[0])
           {
              case '\n':
              {
                 BenutzerEinzelheiten be;
                 be._name = nameGelesen;
                 be._passwort = passwortGelesen;
                 __benutzerListe.push_back( be );
                 nameGelesen = "";
                 passwortGelesen = "";
                 zustandName = true;
              }
              break;
              case ':':
                 zustandName = false;
              break;
              case '\r':
              break;
              default:
                 if( zustandName )
                 {
                    nameGelesen += puffer[0]; 
                 }               
                 else
                 {
                    passwortGelesen += puffer[0]; 
                 }
           }
       }
       close(henkelNL);    
    }
     
    
    
    
    /* Build our SSL context*/
    cout <<"schritt1"<<endl;
    ctx=initialize_ctx("unsinn","1111");
    cout <<"schritt2"<<endl;
    load_dh_params(ctx,"unsinn");
   
    sock=tcp_listen();

    while(1){
      if((s=accept(sock,0,0))<0)
        err_exit("Problem accepting");

      if((pid=fork())){
        close(s);
      }
      else {
        sbio=BIO_new_socket(s,BIO_NOCLOSE);
        ssl=SSL_new(ctx);
        SSL_set_bio(ssl,sbio,sbio);
        
        if((r=SSL_accept(ssl)<=0))
          berr_exit("SSL accept error");
        
        http_serve(ssl,s);
        exit(0);
      }
    }
    destroy_ctx(ctx);
    exit(0);
  }
