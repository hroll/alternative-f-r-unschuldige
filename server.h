#ifndef _server_h
#define _server_h

#define KEYFILE "server.pem"
#define PASSWORD "password"
#define DHFILE "dh1024.pem"

#ifdef __cplusplus
extern "C" int tcp_listen(void);
extern "C" void load_dh_params(SSL_CTX *ctx,char *file);
extern "C" void generate_eph_rsa_key(SSL_CTX *ctx);
#endif

#endif

